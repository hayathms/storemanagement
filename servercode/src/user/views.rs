use rustc_serialize::json;
use iron::{Request, Response, IronResult, status, Plugin};
use common::{ add_base, DbConnection };
//use router::Router;
use std::collections::HashMap;
//use database::queries::get_sql;
use postgres::{Connection};
//use postgres::error::Error as Perror;

use bodyparser;

use database::models::base::BaseModel;

//use database::models::usertable::{ UserTable };

pub fn register(request: &mut Request) -> IronResult<Response> {



    let json_body = request.get::<bodyparser::Json>().unwrap().unwrap();

    let connection: &Connection = request.extensions.get::<DbConnection>().unwrap();
    let mut errors: HashMap<&str, &str> = HashMap::new();



    let email = match json_body.get("email") {
        Some(email) =>{
            let email = email.as_str().unwrap().trim();
            if email.len() == 0 {
                None
            } else {
                Some(email)
            }
        },
        None => None
    }; 

    if email == None {
        errors.insert("Name", "Name cannot be empty.");
        let encoded = json::encode(&errors).unwrap();
        return Ok(Response::with((status::BadRequest, encoded)))
    }

    let password = match json_body.get("password") {
        Some(password) =>{
            let password = password.as_str().unwrap().trim();
            if password.len() == 0 {
                None
            } else {
                Some(password)
            }
        },
        None => None
    }; 

    if password == None {
        errors.insert("Password", "Password cannot be empty.");
        let encoded = json::encode(&errors).unwrap();
        return Ok(Response::with((status::BadRequest, encoded)))
    }

    let new_base: BaseModel;

    let base_id = match json_body["base"].get("id") {
        Some(id) => Some(id.as_i64().unwrap() as i32),
        None => None
    }; 

    if base_id != None {
        new_base = BaseModel::get_base(connection, base_id.unwrap());
    } else {
        new_base = add_base(request);
    }

    println!("Hi Hello this is user mod {:?}", new_base);


    //let name = name.unwrap().to_string();

    //let mut new_store = StoreModel {
    //    id: store_id,
    //    base: new_base.clone(),
    //    name: name.clone(),
    //};

    //match &connection.query(get_sql("storeExists"), &[&name]) {
    //    &Result::Ok(ref rows) => {
    //        if rows.len() > 0 {
    //            let id: i32 = rows.get(0).get("id");
    //            new_store.id = Some(id)
    //        }
    //    },
    //    &Result::Err(_) => (),
    //}

    //match new_store.save_or_update(connection) {
    //    Ok(new_store) => new_store,
    //    Err(error) => panic!("Error: {}", error)
    //};
    Ok(Response::with((status::Ok, "Success")))
    // let encoded = json::encode(&new_store).unwrap();
    // Ok(Response::with((status::Ok, encoded)))
}
