//use mount::Mount;
use router::Router;

use user::views as user_views;


pub fn get_urls() -> Router {

    let mut router = Router::new();
    router.post("/register", user_views::register, "user_register");

    return router
}


