extern crate rustc_serialize;
extern crate iron;
extern crate router;
extern crate mount;
extern crate postgres;
extern crate database;
extern crate bodyparser;
extern crate persistent;
extern crate hyper;
extern crate staticfile;

mod urls;
mod common;
mod store;
mod user;

use iron::Iron;
use iron::Chain;
use common::DbConnection;
use persistent::Read;

const MAX_BODY_LENGTH: usize = 1024 * 1024 * 10;

fn main() {
    let mounted = urls::get_urls();
    let mut chain = Chain::new(mounted);
    
    chain.link_before(DbConnection);
    chain.link_before(Read::<bodyparser::MaxBodyLength>::one(MAX_BODY_LENGTH));
    chain.link_after(DbConnection);

    let url = "139.59.40.241:8020";
    //let url = "127.0.0.1:8010";
    println!("http://{}", url);
    Iron::new(chain).http(url).unwrap();
}
