use postgres::{Connection, TlsMode};
use iron::{Request, Response, IronResult,
           BeforeMiddleware, AfterMiddleware,
           typemap, /*status*/};
use database::models::base::BaseModel;

//use std::io::Read;
//use std::io;
//use std::fs::File;
//use hyper::header::{Headers, ContentType};
//use hyper::mime::{Mime, TopLevel, SubLevel};


pub fn establish_connection() -> Connection {
    let database_url="postgres://hayathms:Ilovesuse@localhost/store";
    
    let conn = Connection::connect(database_url, TlsMode::None);

    let conn: Connection = match conn {
        Err(error) => panic!("\n\n1. Database connection could not be established !\n\n\n {:?}", error),
        Ok(value) => value,
    };
    conn
}

pub fn add_base(request: &Request) -> BaseModel {
    let connection: &Connection = request.extensions.get::<DbConnection>().unwrap();

    let new_base: BaseModel = BaseModel {
        id: None,
        ip_request: request.remote_addr.to_string(),
        is_active: Some(true),
        is_deleted: Some(false),
        created_on: None,
        modified_on: None,
        created_by_id: 21,
    };
    let new_base: BaseModel = new_base.save(connection);
    new_base
}



pub struct DbConnection;

impl typemap::Key for DbConnection { type Value = Connection; }

impl BeforeMiddleware for DbConnection {
    fn before(&self, req: &mut Request) -> IronResult<()> {
        req.extensions.insert::<DbConnection>(establish_connection());
        Ok(())
    }
}

impl AfterMiddleware for DbConnection {
    fn after(&self, _: &mut Request, res: Response) -> IronResult<Response> {
        // let connection: &Connection = req.extensions.get::<DbConnection>().unwrap();
        // for row in &connection.query("SELECT id FROM base", &[]).unwrap() {
        //     let id: i32 = row.get(0);
        //     println!("{}", id);
        // }
        Ok(res)
    }
}


//pub fn index(_: &mut Request) -> IronResult<Response> {
//
//    let file = "../clientcode/index.html".to_string();
//    let file: String = read_file(&file).unwrap();
//
//    let mut respo = Response::with((status::Ok, file.as_bytes()));
//    respo.headers = Headers::new();
//    respo.headers.set(ContentType(Mime(TopLevel::Text, SubLevel::Html, vec![])));
//    Ok(respo)
//}
//
//
//fn read_file(path: &String) -> io::Result<String> {
//
//    let mut f = File::open(path).unwrap();
//    let mut template = String::new();
//    try!(f.read_to_string(&mut template));
//    Ok(template)
//}



