use mount::Mount;
use router::Router;
//use common::index;
use staticfile::Static;
use std::path::Path;

use user;
use store;

pub fn get_urls() -> Mount {

    let mut mount = Mount::new();
    //mount.mount("/", index);

    let store_url: Router = store::urls::get_urls();
    mount.mount("/server/stores/", store_url);

    let user_url: Router = user::urls::get_urls();
    mount.mount("/server/user/", user_url);

    mount.mount("/", Static::new(Path::new("../clientcode/")));
    mount
}

