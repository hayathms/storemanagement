use rustc_serialize::json;
use iron::{Request, Response, IronResult, status, Plugin};
use common::{ add_base, DbConnection };
use router::Router;
use std::collections::HashMap;
use database::queries::get_sql;
use postgres::{Connection};
use postgres::error::Error as Perror;

use bodyparser;

use database::models::store::{ StoreModel };
use database::models::storehotel::{ StoreHotelModel };
use database::models::storeitems::{ StoreItemsModel, StoreResultModel };
use database::models::stockdetails::{ StockDetailsModel, StockDetailsResult };

use database::models::base::BaseModel;


pub fn stores(request: &mut Request) -> IronResult<Response> {

    let connection: &Connection = request.extensions.get::<DbConnection>().unwrap();
    let mut store_vector: Vec<StoreModel> = vec![];

    for row in &connection.query(get_sql("storeList") , &[]).unwrap() {

        let base_model: BaseModel = BaseModel {
            id: row.get("base_id"),
            is_active: row.get("is_active"),
            is_deleted: row.get("is_deleted"),
            created_on: row.get("created_on"),
            modified_on: row.get("modified_on"),
            ip_request: row.get("ip_request"),
            created_by_id: row.get("created_by_id")
        };

        let store: StoreModel = StoreModel {
            id: Option::Some(row.get("id")),
            base: base_model,
            name: row.get("name")
        };

        store_vector.push(store);
    }
    let encoded = json::encode(&store_vector).unwrap();
    Ok(Response::with((status::Ok, encoded)))
}

pub fn add_store(request: &mut Request) -> IronResult<Response> {

    let json_body = request.get::<bodyparser::Json>().unwrap().unwrap();
    let connection: &Connection = request.extensions.get::<DbConnection>().unwrap();
    let mut errors: HashMap<&str, &str> = HashMap::new();

    let store_id = match json_body.get("id") {
        Some(id) => Some(id.as_i64().unwrap() as i32),
        None => None
    };

    let base_id = match json_body["base"].get("id") {
        Some(id) => Some(id.as_i64().unwrap() as i32),
        None => None
    }; 

    let name = match json_body.get("name") {
        Some(name) =>{
            let name = name.as_str().unwrap().trim();
            if name.len() == 0 {
                None
            } else {
                Some(name)
            }
        },
        None => None
    }; 

    if name == None {
        errors.insert("Name", "Name cannot be empty.");
        let encoded = json::encode(&errors).unwrap();
        return Ok(Response::with((status::BadRequest, encoded)))
    }

    let new_base: BaseModel;

    if base_id != None {
        new_base = BaseModel::get_base(connection, base_id.unwrap());
    } else {
        new_base = add_base(request);
    }

    let name = name.unwrap().to_string();

    let mut new_store = StoreModel {
        id: store_id,
        base: new_base.clone(),
        name: name.clone(),
    };

    match &connection.query(get_sql("storeExists"), &[&name]) {
        &Result::Ok(ref rows) => {
            if rows.len() > 0 {
                let id: i32 = rows.get(0).get("id");
                new_store.id = Some(id)
            }
        },
        &Result::Err(_) => (),
    }

    match new_store.save_or_update(connection) {
        Ok(new_store) => new_store,
        Err(error) => panic!("Error: {}", error)
    };
    Ok(Response::with((status::Ok, "Success")))
    // let encoded = json::encode(&new_store).unwrap();
    // Ok(Response::with((status::Ok, encoded)))
}

pub fn register_store_item(request: &mut Request) -> IronResult<Response> {

    let json_body = request.get::<bodyparser::Json>().unwrap().unwrap();
    let connection: &Connection = request.extensions.get::<DbConnection>().unwrap();
    let mut errors: HashMap<&str, &str> = HashMap::new();

    let store_id = request.extensions.get::<Router>().unwrap().find("store_id").unwrap();
    let store_id = store_id.parse::<i32>().unwrap();

    let store_item_id = match json_body.get("id") {
        Some(id) => Some(id.as_i64().unwrap() as i32),
        None => None
    };

    let base_id = match json_body["base"].get("id") {
        Some(id) => Some(id.as_i64().unwrap() as i32),
        None => None
    }; 

    let name = match json_body.get("name") {
        Some(name) =>{
            let name = name.as_str().unwrap().trim();
            if name.len() == 0 {
                None
            } else {
                Some(name)
            }
        },
        None => None
    }; 

    let unit = match json_body.get("unit") {
        Some(name) =>{
            let name = name.as_str().unwrap().trim();
            if name.len() == 0 {
                None
            } else {
                Some(name)
            }
        },
        None => None
    };

    if (name == None) | (unit == None) {
        errors.insert("Name Or Unit", "Cannot be empty.");
        let encoded = json::encode(&errors).unwrap();
        return Ok(Response::with((status::BadRequest, encoded)))
    } 

    let new_base: BaseModel;

    if base_id != None {
        new_base = BaseModel::get_base(connection, base_id.unwrap());
    } else {
        new_base = add_base(request);
    }

    let cost = match json_body.get("cost") {
        Some(x) => x.as_f64().unwrap(),
        None => 0.0f64
    };

    let total_stock = match json_body.get("total_stock") {
        Some(x) => x.as_f64().unwrap(),
        None => 0.0f64
    };

    let mut new_store_item = StoreItemsModel {
        id: store_item_id,
        store_id: store_id,
        base: new_base.clone(),
        name: name.unwrap().to_string(),
        unit: unit.unwrap().to_string(),
        cost: cost,
        total_stock: total_stock
    };

    for name in new_store_item.name.split(',') {

        if name.trim().len() == 0 {
            continue;
        }
        match &connection.query(get_sql("itemExists"), &[&name, &store_id]) {
            &Result::Ok(ref rows) => {
                if rows.len() > 0 {
                    let id: i32 = rows.get(0).get("id");
                    new_store_item.id = Some(id)
                }
            },
            &Result::Err(_) => (),
        }
        let new_store_item = StoreItemsModel {
            name: name.to_string(),
            .. new_store_item.clone()
        };

        match new_store_item.save_or_update(connection) {
            Ok(new_store_item) => new_store_item,
            Err(error) => panic!("Error: {}", error)
        };
    }

    Ok(Response::with((status::Ok, "Success")))
}

pub fn add_store_to_hotel(request: &mut Request) -> IronResult<Response> {

    let connection: &Connection = request.extensions.get::<DbConnection>().unwrap();
    let hotel_id = request.extensions.get::<Router>().unwrap().find("hotel").unwrap_or("/");
    let hotel_id: i32 = hotel_id.parse::<i32>().unwrap();
    let name = request.extensions.get::<Router>().unwrap().find("name").unwrap_or("/");
    //let store_hotel_obj = StoreHotelModel::get_existing(&hotel_id, &name, connection);

    let store = match StoreHotelModel::get_existing(&hotel_id, &name, connection) {
        Ok(storehotel) => store_exiting(request, storehotel),
        Err(_) => add_new_store_to_hotel(request, name, hotel_id)
    };

    let encoded = json::encode(&store).unwrap();
    Ok(Response::with((status::Ok, encoded)))
}

fn add_new_store_to_hotel(request: &Request, name: &str, hotel_id: i32) -> StoreModel {

    let connection: &Connection = request.extensions.get::<DbConnection>().unwrap();
    let new_base: BaseModel = add_base(request);
    let new_store = StoreModel {
        id: None,
        base: new_base.clone(),
        name: name.to_string(),
    };
    let new_store: StoreModel = new_store.save(connection).unwrap();
    let store_hotel_map = StoreHotelModel {
        id: None,
        hotel_id: hotel_id,
        base_id: new_base.id.unwrap(),
        store_id: new_store.id.unwrap()
    };
    store_hotel_map.save(connection);
    new_store
}

fn store_exiting(request: &Request, storehotel: StoreHotelModel) -> StoreModel {
    let connection: &Connection = request.extensions.get::<DbConnection>().unwrap();
    let store = StoreModel::get(&storehotel.id.unwrap(), connection);
    store.unwrap()
}


pub fn store_list(request: &mut Request) -> IronResult<Response> {

    let connection: &Connection = request.extensions.get::<DbConnection>().unwrap();

    let hotel_id = request.extensions.get::<Router>().unwrap().find("hotel").unwrap_or("/");
    let hotel_id: i32 = hotel_id.parse::<i32>().unwrap();

    let mut store_vector: Vec<HashMap<&str, String>> = vec![];
    for row in &connection.query(get_sql("getStoreList"), &[&hotel_id]).unwrap() {

        let mut store_model: HashMap<&str, String> = HashMap::new();

        let id: i32 =  row.get("id");
        let name: String =  row.get("name");

        store_model.insert("id", id.to_string());
        store_model.insert("name", name);

        store_vector.push(store_model);
    }
    let encoded = json::encode(&store_vector).unwrap();
    Ok(Response::with((status::Ok, encoded)))
}


pub fn store_hotel_list(request: &mut Request) -> IronResult<Response> {

    let json_body = request.get::<bodyparser::Json>().unwrap().unwrap();
    let mut errors: HashMap<&str, &str> = HashMap::new();

    let hotel_id = match json_body.get("hotel_id") {
        Some(called) => Some(called.as_i64().unwrap() as i32),
        None => {errors.insert("hotel_id", "Hotel ID Needed");None}
    };

    let mut stores_ids: Vec<i32> = vec![];

    let store_id_a = match json_body.get("stores_ids") {
        Some(element) => Some(element.as_array().unwrap()),
        None => {errors.insert("stores_ids", "Store ID's Needed."); None}
    };

    if errors.len() > 0 {
        let encoded = json::encode(&errors).unwrap();
        return Ok(Response::with((status::Ok, encoded)))
    }

    for id in store_id_a.unwrap() {
        stores_ids.push(id.as_i64().unwrap() as i32);
    }

    let connection: &Connection = request.extensions.get::<DbConnection>().unwrap();
    let mut store_vector: HashMap<i32, String> = HashMap::new();
    for row in &connection.query(get_sql("getData3"), &[&hotel_id, &stores_ids]).unwrap() {
        let id: i32 = row.get("id");
        let name: String = row.get("name");
        store_vector.insert(id, name);
    }
    let encoded = json::encode(&store_vector).unwrap();
    Ok(Response::with((status::Ok, encoded)))
}

pub fn item_list(request: &mut Request) -> IronResult<Response> {

    let connection: &Connection = request.extensions.get::<DbConnection>().unwrap();
    let store_id = request.extensions.get::<Router>().unwrap().find("store_id").unwrap();
    let store_id = store_id.parse::<i32>().unwrap();

    let mut store_vector: Vec<StoreItemsModel> = vec![];

    for row in &connection.query(get_sql("getItemList"), &[&store_id]).unwrap() {

        let base: BaseModel = BaseModel::get_base(connection, row.get("base_id"));

        let store_item = StoreItemsModel {
            id: Some(row.get("id")),
            name: row.get("name"),
            store_id: row.get("store_id"),
            base: base,
            unit: row.get("unit"),
            cost: row.get("cost"),
            total_stock: row.get("total_stock")
        };
        store_vector.push(store_item);
    }
    let encoded = json::encode(&store_vector).unwrap();
    Ok(Response::with((status::Ok, encoded)))
}

pub fn register_stock_item(request: &mut Request) -> IronResult<Response> {

    let json_body = request.get::<bodyparser::Json>().unwrap().unwrap();
    let connection: &Connection = request.extensions.get::<DbConnection>().unwrap();
    let mut errors: HashMap<&str, &str> = HashMap::new();

    //let store_id = request.extensions.get::<Router>().unwrap().find("store_id").unwrap().parse::<i32>().unwrap();

    let storeitems_id = match json_body.get("id") {
        Some(id) => Some(id.as_i64().unwrap() as i32),
        None => None
    };
    let cost = match json_body.get("price") {
        Some(x) => x.as_f64().unwrap(),
        None => 0.0f64
    };
    let qty = match json_body.get("qty") {
        Some(x) => x.as_f64().unwrap(),
        None => 0.0f64
    };

    if (storeitems_id == None) | (qty == 0.0f64){
        errors.insert("Name Or Quantity", "Cannot be empty.");
        let encoded = json::encode(&errors).unwrap();
        return Ok(Response::with((status::BadRequest, encoded)))
    } 

    match add_to_storeitem(qty, cost, storeitems_id.unwrap(), connection) {
        Ok(_) => (),
        Err(x) => panic!("{:?}", x),
    };

    let new_base: BaseModel = add_base(request);

    let stock_details: StockDetailsModel = StockDetailsModel {
        id: None,
        storeitems_id: storeitems_id.unwrap(),
        base_id: new_base.id.unwrap(),
        cost: cost,
        qty: qty,
        route: "A".to_string()
    };
    match stock_details.save(connection) {
        Ok(_) => (),
        Err(message) => panic!(message)
    };
    Ok(Response::with((status::Ok, "Success")))
}

fn add_to_storeitem(qty: f64, cost: f64, storeitem_id: i32, connection: &Connection) -> Result<StoreItemsModel, Perror> {
    let mut store_model = try!(StoreItemsModel::get(&storeitem_id, connection));
    store_model.total_stock = store_model.total_stock + qty;
    if cost > 0.0f64 {
        store_model.cost = cost/qty;
    }
    store_model.update(connection)
}


pub fn ui_transaction_list(request: &mut Request) -> IronResult<Response> {

    let connection: &Connection = request.extensions.get::<DbConnection>().unwrap();
    let store_id = request.extensions.get::<Router>().unwrap().find("store_id").unwrap().parse::<i32>().unwrap();
    let item_id = request.extensions.get::<Router>().unwrap().find("item_id").unwrap().parse::<i32>().unwrap();
    //let page_no = request.extensions.get::<Router>().unwrap().find("page_no").unwrap().parse::<i32>().unwrap();

    let mut stock_maual: Vec<StockDetailsResult> = vec![];
    for row in &connection.query(get_sql("manualTransaction"), &[&store_id, &item_id]).unwrap() {
        let stock_details: StockDetailsResult = StockDetailsResult {
            id: Some(row.get("id")),
            qty: row.get("qty"),
            cost: row.get("cost"),
            storeitems_id: row.get("storeitems_id"),
            base_id: row.get("base_id"),
            created_on: row.get("created_on")
        };
        stock_maual.push(stock_details);
    }
    let data = ("manual_transaction", stock_maual);
    let encoded = json::encode(&data).unwrap();
    Ok(Response::with((status::Ok, encoded)))
}

/// this method reurtns list of stores and items linked to that product
pub fn get_store_itms(request: &mut Request) -> IronResult<Response> {

    let connection: &Connection = request.extensions.get::<DbConnection>().unwrap();
    let hotel_id = request.extensions.get::<Router>().unwrap().find("hotel_id").unwrap().parse::<i32>().unwrap();
    let mut stores: HashMap<i32, StoreResultModel> = HashMap::new();

    for row in &connection.query(get_sql("storeItemsApi"), &[&hotel_id]).unwrap() {

        let storeitem_base_id: Option<i32> = row.get("storeitem_base_id");
        let store_item: Option<StoreItemsModel> = match storeitem_base_id {
            Some(_) => {
                let storeitem_base = BaseModel {
                    id: Option::Some(row.get("storeitem_base_id")),
                    ip_request: row.get("storeitem_base_ip_request"),
                    created_by_id: row.get("storeitem_base_created_by_id"),
                    is_active: Some(row.get("storeitem_base_is_active")),
                    is_deleted: Some(row.get("storeitem_base_is_deleted")),
                    created_on: Some(row.get("storeitem_base_created_on")),
                    modified_on: Some(row.get("storeitem_base_modified_on"))
                };

                let store_item = StoreItemsModel {
                    id: Some(row.get("storeitem_id")),
                    name: row.get("storeitem_name"),
                    cost: row.get("storeitem_cost"),
                    unit: row.get("storeitem_unit"),
                    store_id: row.get("storeitem_store_id"),
                    total_stock: row.get("total_stock"),
                    base: storeitem_base
                };
                Some(store_item)
            },
            None => None
        };

        let store_id: i32 = row.get("store_id");
        let store = match stores.get_mut(&store_id) {
            Some(store) => {
                match store_item {
                    Some(st_item) => {
                        store.items.push(st_item);
                        ()
                    },
                    None => ()
                };
                None
            },
            None =>  {
                let store_base = BaseModel {
                    id: Option::Some(row.get("store_base_id")),
                    ip_request: row.get("store_base_ip_request"),
                    created_by_id: row.get("store_base_created_by_id"),
                    is_active: Some(row.get("store_base_is_active")),
                    is_deleted: Some(row.get("store_base_is_deleted")),
                    created_on: Some(row.get("store_base_created_on")),
                    modified_on: Some(row.get("store_base_modified_on"))
                };

                let st = StoreResultModel {
                    id: Some(row.get("store_id")),
                    base: store_base,
                    name: row.get("store_name"),
                    items: match store_item {
                        Some(st_item) => vec![st_item],
                        None => vec![]
                    }
                };
                Some(st)
            }
        };
        match store {
            Some(st) => {
                stores.insert(st.id.unwrap(), st);
            },
            None => ()
        }
    }
    let encoded = json::encode(&stores).unwrap();
    Ok(Response::with((status::Ok, encoded)))
}

