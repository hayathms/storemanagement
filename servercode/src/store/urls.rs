//use mount::Mount;
use router::Router;

use store::views as store_views;


pub fn get_urls() -> Router {

    //let mut mount = Mount::new();
    let mut router = Router::new();
    router.get("/", store_views::stores, "stores");
    router.post("/add/", store_views::add_store, "add_store");
    router.get("/complete-store-items/:hotel_id", store_views::get_store_itms, "get_store_itms");

    router.post("/:store_id/store-item/register/", store_views::register_store_item, "register_store_item");
    router.post("/:store_id/store-item/manual-transaction/", store_views::register_stock_item, "register_stock_item");

    router.get("/:store_id/ui-transaction-list/:item_id/:page_no/", store_views::ui_transaction_list, "ui_transaction_list");

    router.get("/:store_id/item-list/", store_views::item_list, "item_list");
    router.get("/add/:hotel/:name", store_views::add_store_to_hotel, "add_store_to_hotel");
    router.get("/list/:hotel", store_views::store_list, "store_list_hotel");
    router.post("/list/", store_views::store_hotel_list, "store_list_h");

    router
}


