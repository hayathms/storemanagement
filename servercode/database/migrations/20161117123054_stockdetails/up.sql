CREATE TABLE stockdetails
(
    id serial PRIMARY KEY NOT NULL,
    qty DOUBLE PRECISION NOT NULL,
    storeitems_id INTEGER REFERENCES storeitems,
    cost DOUBLE PRECISION NOT NULL,
    base_id integer REFERENCES base,
    route VARCHAR(1)
);

