CREATE TABLE store
(
  id serial PRIMARY KEY NOT NULL,
  base_id integer REFERENCES base,
  name VARCHAR(200) NOT NULL
);
