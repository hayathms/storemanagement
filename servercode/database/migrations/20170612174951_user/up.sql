CREATE TABLE usertable (
    id serial PRIMARY KEY NOT NULL,
    email VARCHAR(100) NOT NULL UNIQUE,
    password VARCHAR(200) NOT NULL,
    base_id integer REFERENCES base,
    name VARCHAR(200) 
);

