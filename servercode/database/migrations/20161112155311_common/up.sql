CREATE TABLE base
(
  id serial PRIMARY KEY NOT NULL,
  is_active boolean DEFAULT 't' NOT NULL,
  created_on timestamp with TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP,
  modified_on timestamp with TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP,
  is_deleted boolean DEFAULT 'f' NOT NULL,
  ip_request VARCHAR(50) NOT NULL,
  created_by_id integer NOT NULL
);

CREATE OR REPLACE FUNCTION update_changetimestamp_column()
RETURNS TRIGGER AS $$
BEGIN
   NEW.modified_on = now(); 
   RETURN NEW;
END;
$$ language 'plpgsql';

CREATE TRIGGER update_ab_changetimestamp BEFORE UPDATE
   ON base FOR EACH ROW EXECUTE PROCEDURE 
   update_changetimestamp_column();

