
CREATE TABLE store_hotel
(
  id serial PRIMARY KEY NOT NULL,
  hotel_id integer NOT NULL,
  store_id integer REFERENCES store,
  base_id integer REFERENCES base
);

