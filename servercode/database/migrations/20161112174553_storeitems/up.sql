CREATE TABLE storeitems
(
  id serial PRIMARY KEY NOT NULL,
  base_id integer REFERENCES base,
  store_id integer REFERENCES store,
  name VARCHAR(200),
  unit VARCHAR(3) NOT NULL,
  total_stock DOUBLE PRECISION NOT NULL,
  cost DOUBLE PRECISION NOT NULL
);
