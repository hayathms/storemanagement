use std::io::{Error, ErrorKind};
use postgres::Connection;
use postgres::error::Error as Perror;
use chrono::{ DateTime, UTC };

#[derive(Debug, Clone, RustcDecodable, RustcEncodable)]
pub struct StockDetailsModel {
    pub id: Option<i32>,
    pub qty: f64,
    pub storeitems_id: i32,
    pub cost: f64,
    pub base_id: i32,
    pub route: String,
}

#[derive(Debug, Clone, RustcDecodable, RustcEncodable)]
pub struct StockDetailsResult {
    pub id: Option<i32>,
    pub qty: f64,
    pub storeitems_id: i32,
    pub cost: f64,
    pub base_id: i32,
    pub created_on: DateTime<UTC>
}

impl StockDetailsModel {

    pub fn save(&self, connection: &Connection) -> Result<StockDetailsModel, Perror> {
        let result = try!(connection.query("INSERT INTO stockdetails
                    (qty, storeitems_id, cost, base_id, route) VALUES ($1, $2, $3, $4, $5) RETURNING id",
                 &[&self.qty, &self.storeitems_id, &self.cost, &self.base_id, &self.route]));
        let id: i32 = result.get(0).get("id");

        let stock_model = StockDetailsModel {
                id: Option::Some(id),
                qty: self.qty,
                storeitems_id: self.storeitems_id,
                cost: self.cost,
                base_id: self.base_id,
                route: self.route.clone()
            };
        Ok(stock_model)
    }

    pub fn save_or_update(&self, connection: &Connection) -> Result<StockDetailsModel, Perror> {
        match self.id {
            Some(_) => self.update(),
            None => self.save(connection)
        }
    }

    pub fn update(&self) -> Result<StockDetailsModel, Perror> {
        // Need to impliment
        Err(Perror::Io(Error::new(ErrorKind::Other, "oh no!")))
    }
}


