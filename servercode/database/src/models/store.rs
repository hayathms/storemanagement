use postgres::Connection;
use postgres::error::Error as Perror;
use models::base::BaseModel;
use queries::get_sql;
use chrono::{DateTime, UTC};


#[derive(Debug, Clone, RustcDecodable, RustcEncodable)]
pub struct StoreModel {
    pub id: Option<i32>,
    pub base: BaseModel,
    pub name: String
}


impl StoreModel {

    pub fn save(&self, connection: &Connection) -> Result<StoreModel, Perror> {
        let result = try!(connection.query("INSERT INTO store (base_id, name) VALUES
                                      ($1, $2) RETURNING id",
                 &[&self.base.id.unwrap(), &self.name]));
        let id: i32 = result.get(0).get("id");

        let store_model = StoreModel {
            id: Option::Some(id),
            base: self.base.clone(),
            name: (*self.name).to_string(),
        };
        Ok(store_model)
    }

    pub fn save_or_update(&self, connection: &Connection) -> Result<StoreModel, Perror> {
        match self.id {
            Some(_) => self.update(connection),
            None => self.save(connection)
        }
    }

    pub fn update(&self, connection: &Connection) -> Result<StoreModel, Perror> {
        
        try!(connection.query("UPDATE store SET name=$1 WHERE id=$2",
            &[&self.name, &self.id.unwrap()]));

        let today: DateTime<UTC> = UTC::now();

        try!(connection.query("UPDATE base SET modified_on=$1 WHERE id=$2",
            &[&today, &self.base.id.unwrap()]));

        let store_model = StoreModel {
            id: self.id.clone(),
            base: self.base.clone(),
            name: (*self.name).to_string(),
        };
        Ok(store_model)
    }

    pub fn get(id: &i32, connection: &Connection) -> Result<StoreModel, Perror> {
        let rows = try!(connection.query(get_sql("getStore") , &[id]));
        let store_obj: StoreModel = StoreModel {
            id : Some(rows.get(0).get("id")),
            name: rows.get(0).get("name"),
            base: BaseModel::get_base(connection, rows.get(0).get("base_id"))
        };
        Ok(store_obj)
    }
}

