use postgres::Connection;
use postgres::error::Error as Perror;
use queries::get_sql;
use models::base::BaseModel;
use chrono::{DateTime, UTC};

#[derive(Debug, Clone, RustcDecodable, RustcEncodable)]
pub struct StoreHotelModel {
    pub id: Option<i32>,
    pub store_id: i32,
    pub hotel_id: i32,
    pub base_id: i32
}

impl StoreHotelModel {
    pub fn save(&self, connection: &Connection) -> StoreHotelModel {
        let result = connection.query("INSERT INTO store_hotel (hotel_id, store_id, base_id) VALUES
                                      ($1, $2, $3) RETURNING id",
                 &[&self.hotel_id, &self.base_id, &self.store_id]).unwrap();
        let id: i32 = result.get(0).get("id");
        let store_hotel = self.clone();
        StoreHotelModel { id: Some(id), ..store_hotel }
    }

    pub fn get(id: &i32, connection: &Connection) -> Result<StoreHotelModel, Perror> {

        let rows = try!(connection.query(get_sql("getStoreHotelItem") , &[id]));
        let store_hotel_obj: StoreHotelModel = StoreHotelModel{
            id : Some(rows.get(0).get("id")),
            store_id: rows.get(0).get("store_id"),
            hotel_id: rows.get(0).get("hotel_id"),
            base_id: rows.get(0).get("store_id")
        };
        Ok(store_hotel_obj)
    }

    pub fn get_existing(hotel_id: &i32, name: &str, connection: &Connection) -> Result<StoreHotelModel, i32 > {

        let rows = match connection.query(get_sql("storeHotelExists") , &[hotel_id, &name]) {
            Ok(row) => row,
            Err(_) => panic!("Database Broke")
        };
        if rows.len() == 0 {
            return Err(0)
        }
        let store_hotel_obj: StoreHotelModel = StoreHotelModel{
            id : Some(rows.get(0).get("id")),
            store_id: rows.get(0).get("store_id"),
            hotel_id: rows.get(0).get("hotel_id"),
            base_id: rows.get(0).get("base_id")
        };
        Ok(store_hotel_obj)
    }
}

