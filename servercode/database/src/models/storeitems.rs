use postgres::Connection;
use models::base::BaseModel;
use queries::get_sql;
use chrono::{DateTime, UTC};
use postgres::error::Error as Perror;

#[derive(Debug, Clone, RustcDecodable, RustcEncodable)]
pub struct StoreItemsModel {
    pub id: Option<i32>,
    pub store_id: i32,
    pub base: BaseModel,
    pub name: String,
    pub unit: String,
    pub total_stock: f64,
    pub cost: f64
}

impl StoreItemsModel {

    pub fn save(&self, connection: &Connection) -> Result<StoreItemsModel, Perror> {
        let result = try!(connection.query("INSERT INTO storeitems
                    (base_id, name, store_id, unit, cost, total_stock) VALUES ($1, $2, $3, $4, $5, $6) RETURNING id",
                 &[&self.base.id.unwrap(), &self.name, &self.store_id, &self.unit, &self.cost, &self.total_stock]));
        let id: i32 = result.get(0).get("id");

        let store_model = StoreItemsModel {
                id: Option::Some(id),
                store_id: self.store_id.clone(),
                base: self.base.clone(),
                name: (*self.name).to_string(),
                unit: (*self.unit).to_string(),
                cost: self.cost,
                total_stock: self.total_stock
            };
        Ok(store_model)
    }

    pub fn save_or_update(&self, connection: &Connection) -> Result<StoreItemsModel, Perror> {
        match self.id {
            Some(_) => self.update(connection),
            None => self.save(connection)
        }
    }

    pub fn update(&self, connection: &Connection) -> Result<StoreItemsModel, Perror> {

        try!(connection.query("UPDATE storeitems SET unit=$1, total_stock=$2, cost=$3 WHERE id=$4",
            &[&self.unit, &self.total_stock, &self.cost, &self.id.unwrap()]));

        let today: DateTime<UTC> = UTC::now();

        try!(connection.query("UPDATE base SET modified_on=$1 WHERE id=$2", 
            &[&today, &self.base.id.unwrap()]));

        let store_model = StoreItemsModel {
            id: self.id.clone(),
            store_id: self.store_id.clone(),
            base: self.base.clone(),
            name: (*self.name).to_string(),
            unit: (*self.unit).to_string(),
            cost: self.cost,
            total_stock: self.total_stock
        };
        Ok(store_model)
    }


    pub fn get(id: &i32, connection: &Connection) -> Result<StoreItemsModel, Perror> {
        let rows = try!(connection.query(get_sql("getStoreItem") , &[id]));
        let store_obj: StoreItemsModel = StoreItemsModel{
            id : Some(rows.get(0).get("id")),
            name: rows.get(0).get("name"),
            cost: rows.get(0).get("cost"),
            unit: rows.get(0).get("unit"),
            store_id: rows.get(0).get("store_id"),
            total_stock: rows.get(0).get("total_stock"),
            base: BaseModel::get_base(connection, rows.get(0).get("base_id"))
        };
        Ok(store_obj)
    }

}

#[derive(Debug, Clone, RustcDecodable, RustcEncodable)]
pub struct StoreResultModel {
    pub id: Option<i32>,
    pub base: BaseModel,
    pub name: String,
    pub items: Vec<StoreItemsModel>
}


