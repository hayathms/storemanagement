use postgres::Connection;
use postgres::error::Error as Perror;
use models::base::BaseModel;
use queries::get_sql;
use chrono::{DateTime, UTC};


#[derive(Debug, Clone, RustcDecodable, RustcEncodable)]
pub struct Sessions {
    pub id: Option<i32>,
    pub created_on: Option<DateTime<UTC>>,
    pub modified_on: Option<DateTime<UTC>>,
    pub ip_request: String,
    pub usertable_id: i32
}

impl Sessions {

    pub fn save(&self, connection: &Connection) -> Result<Sessions, Perror> {
        let result = try!(connection.query("INSERT INTO sessions (created_on, modified_on,
                                                                  ip_request, usertable_id)
                                            VALUES ($1, $2, $3, $4) RETURNING id",
                        &[&self.created_on.unwrap(), &self.modified_on.unwrap(),
                          &self.ip_request, &self.usertable_id]));

        let id: i32 = result.get(0).get("id");
        let usertable_model = Sessions {
            id: Option::Some(id),
            created_on: self.created_on.clone(),
            modified_on: self.modified_on.clone(),
            ip_request: self.ip_request.clone(),
            usertable_id: i32
        };
        Ok(usertable_model)
    }

    pub fn save_or_update(&self, connection: &Connection) -> Result<Sessions, Perror> {
        match self.id {
            Some(_) => self.update(connection),
            None => self.save(connection)
        }
    }

    pub fn update(&self, connection: &Connection) -> Result<Sessions, Perror> {
        
        let today: DateTime<UTC> = UTC::now();

        try!(connection.query("UPDATE sessions SET modified_on=$1 WHERE id=$2",
            &[&today, &self.base.id.unwrap()]));

        let sessions_model = Sessions {
            id: self.id.clone(),
            created_on: self.created_on.clone(),
            modified_on: self.modified_on.clone(),
            ip_request: self.ip_request.clone(),
            usertable_id: self.usertable_id.clone()
        };
        Ok(sessions_model)
    }

    pub fn get(id: &i32, connection: &Connection) -> Result<Sessions, Perror> {
        let rows = try!(connection.query(get_sql("getSession") , &[id]));
        let session_obj: Sessions = Sessions {
            id : Some(rows.get(0).get("id")),
            created_on: rows.get(0).get("created_on"),
            modified_on: rows.get(0).get("modified_on"),
            ip_request: rows.get(0).get("ip_request")
        };
        Ok(session_obj)
    }
}

