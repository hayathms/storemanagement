use postgres::Connection;
use postgres::error::Error as Perror;
use models::base::BaseModel;
use queries::get_sql;
use chrono::{DateTime, UTC};


#[derive(Debug, Clone, RustcDecodable, RustcEncodable)]
pub struct UserTable {
    pub id: Option<i32>,
    pub base: BaseModel,
    pub email: String,
    pub password: String,
    pub name: Option<String>
}


impl UserTable {

    pub fn save(&self, connection: &Connection) -> Result<UserTable, Perror> {
        let result = try!(connection.query("INSERT INTO usertable (base_id, email, password, name)
                                            VALUES ($1, $2, $3, $4) RETURNING id",
                        &[&self.base.id.unwrap(), &self.email, &self.password, &self.name]));
        let id: i32 = result.get(0).get("id");

        let usertable_model = UserTable {
            id: Option::Some(id),
            base: self.base.clone(),
            name: self.name.clone(),
            email: (*self.email).to_string(),
            password: (*self.email).to_string()
        };
        Ok(usertable_model)
    }

    pub fn save_or_update(&self, connection: &Connection) -> Result<UserTable, Perror> {
        match self.id {
            Some(_) => self.update(connection),
            None => self.save(connection)
        }
    }

    pub fn update(&self, connection: &Connection) -> Result<UserTable, Perror> {
        
        try!(connection.query("UPDATE usertable SET name=$1, password=$2 WHERE id=$3",
            &[&self.name, &self.password, &self.id.unwrap()]));

        let today: DateTime<UTC> = UTC::now();

        try!(connection.query("UPDATE base SET modified_on=$1 WHERE id=$2",
            &[&today, &self.base.id.unwrap()]));

        let usertable_model = UserTable {
            id: self.id.clone(),
            base: self.base.clone(),
            name: self.name.clone(),
            email: (*self.email).to_string(),
            password: (*self.email).to_string()
        };
        Ok(usertable_model)
    }

    pub fn get(id: &i32, connection: &Connection) -> Result<UserTable, Perror> {
        let rows = try!(connection.query(get_sql("getUser") , &[id]));
        let user_obj: UserTable = UserTable {
            id : Some(rows.get(0).get("id")),
            name: Some(rows.get(0).get("name")),
            password: rows.get(0).get("password"),
            email: rows.get(0).get("email"),
            base: BaseModel::get_base(connection, rows.get(0).get("base_id"))
        };
        Ok(user_obj)
    }
}

