use postgres::Connection;
use postgres::rows::Row;
use chrono::{ DateTime, UTC };

#[derive(Debug, Clone,  RustcDecodable, RustcEncodable)]
pub struct BaseModel {
    pub id: Option<i32>,
    pub ip_request: String,
    pub created_by_id: i32,
    pub is_active: Option<bool>,
    pub is_deleted: Option<bool>,
    pub created_on: Option<DateTime<UTC>>,
    pub modified_on: Option<DateTime<UTC>>,
}

impl BaseModel {
    pub fn save(&self, connection: &Connection) -> BaseModel  {
        let result = connection.query("INSERT INTO base (ip_request, created_by_id) VALUES ($1, $2) RETURNING *",
                 &[&self.ip_request, &self.created_by_id]).unwrap();
        let row = result.get(0);
        BaseModel::rebuild(row)
    }

    pub fn rebuild(row: Row) -> BaseModel {
        BaseModel {
                    id: Option::Some(row.get("id")),
                    ip_request: row.get("ip_request"),
                    created_by_id: row.get("created_by_id"),
                    is_active: Some(row.get("is_active")),
                    is_deleted: Some(row.get("is_deleted")),
                    created_on: Some(row.get("created_on")),
                    modified_on: Some(row.get("modified_on"))
        }
    }

    // primary key akways take as i32, others needs more research
    pub fn get_base(connection: &Connection, id :i32) -> BaseModel  {
        let result = connection.query("SELECT * FROM base  WHERE id = $1", &[&id]).unwrap();
        let row = result.get(0);
        BaseModel::rebuild(row)
    }

}
