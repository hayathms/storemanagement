SELECT  st_d.id, st_d.qty, st_d.cost,
        st_d.storeitems_id, st_d.base_id,
        bs.is_active, bs.modified_on,
        bs.is_deleted, bs.ip_request,
        bs.created_by_id, bs.created_on
FROM stockdetails st_d
    INNER JOIN base bs ON st_d.base_id = bs.id
    INNER JOIN storeitems sti ON sti.id = st_d.storeitems_id
WHERE bs.is_active=TRUE AND
    bs.is_deleted=FALSE AND
    sti.store_id=($1) AND
    st_d.storeitems_id=($2);

