SELECT * FROM store_hotel sth
    INNER JOIN base bs ON sth.base_id = bs.id
    INNER JOIN store st ON st.id = sth.store_id
WHERE sth.hotel_id = ($1) AND st.name = ($2)
        
