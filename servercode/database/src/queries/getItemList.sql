SELECT  sti.name, sti.id, sti.unit, sti.cost,
        sti.total_stock total_stock, bs.id base_id,
        st.id store_id

FROM storeitems sti

    INNER JOIN base bs ON sti.base_id = bs.id

    INNER JOIN store st ON sti.store_id = st.id

WHERE st.id = ($1) ORDER BY sti.name
