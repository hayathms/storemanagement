SELECT  
    st.id store_id,
    st.name store_name,

    st_bs.id store_base_id,
    st_bs.ip_request store_base_ip_request,
    st_bs.created_by_id store_base_created_by_id,
    st_bs.is_active store_base_is_active,
    st_bs.is_deleted store_base_is_deleted,
    st_bs.created_on store_base_created_on,
    st_bs.modified_on store_base_modified_on,

    sti.id storeitem_id,
    sti.name storeitem_name,
    sti.cost storeitem_cost,
    sti.unit storeitem_unit,
    sti.store_id storeitem_store_id,
    sti.total_stock total_stock,

    sti_bs.id storeitem_base_id,
    sti_bs.ip_request storeitem_base_ip_request,
    sti_bs.created_by_id storeitem_base_created_by_id,
    sti_bs.is_active storeitem_base_is_active,
    sti_bs.is_deleted storeitem_base_is_deleted,
    sti_bs.created_on storeitem_base_created_on,
    sti_bs.modified_on storeitem_base_modified_on

    FROM store st
    LEFT OUTER JOIN storeitems sti ON st.id = sti.store_id
    LEFT OUTER JOIN base sti_bs ON sti.base_id = sti_bs.id
    INNER JOIN base st_bs ON st.base_id = st_bs.id
    INNER JOIN store_hotel sth ON sth.store_id = st.id
WHERE sth.hotel_id=($1)

