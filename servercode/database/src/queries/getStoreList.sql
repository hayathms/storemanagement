SELECT st.id, st.name, st.base_id, bs.is_active, bs.modified_on,
    bs.is_deleted, bs.ip_request, bs.created_by_id, bs.created_on
FROM store st
    INNER JOIN base bs ON st.base_id = bs.id
    INNER JOIN store_hotel sh ON sh.store_id = st.id
WHERE bs.is_active=TRUE AND
    bs.is_deleted=FALSE AND
    sh.hotel_id=($1)
