SELECT  sti.id id, sti.name, sti.unit, sti.total_stock,
        bs.id base_id, sti.store_id store_id, sti.cost
FROM storeitems sti
    INNER JOIN base bs ON sti.base_id = bs.id
WHERE sti.id = ($1) 
