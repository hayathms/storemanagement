
pub fn get_sql(filename: &str) -> &str {
    match filename {
        "getStoreList" => include_str!("getStoreList.sql"),
        "storeList" => include_str!("storeList.sql"),
        "getData3" => include_str!("getData3.sql"),
        "getItemList" => include_str!("getItemList.sql"),
        "itemExists" => include_str!("itemExists.sql"),
        "storeExists" => include_str!("storeExists.sql"),
        "storeHotelExists" => include_str!("storeHotelExists.sql"),
        "getStoreItem" => include_str!("getStoreItem.sql"),
        "getStore" => include_str!("getStore.sql"),
        "getStoreHotelItem" => include_str!("getStoreHotelItem.sql"),
        "manualTransaction" => include_str!("manualTransaction.sql"),
        "storeItemsApi" => include_str!("storeItemsApi.sql"),
        _          => panic!("\n *File Nor Registered in queries.\n"),
    }

}
