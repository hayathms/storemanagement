SELECT st.id, st.name, st.base_id, bs.is_active, bs.modified_on,
    bs.is_deleted, bs.ip_request, bs.created_by_id, bs.created_on
FROM store st INNER JOIN base bs ON st.base_id = bs.id
